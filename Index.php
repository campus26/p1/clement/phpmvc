<?php
session_start();
require 'controller.php';


if (isset($_GET['Page'])) {


    switch ($_GET['Page']) {
        case 'modif':
            Modif($_GET['id']);
            break;

        case 'suprime':
            Supression($_GET['id']);
            header('Location: ../ressource');
            break;

        case 'formulaire':
            afficheForm();
            break;

        case 'connect':
            AfficheLogin();
            break;

        case 'deconnect':
            session_destroy();
            header('Location: ressource');
            break;

        case 'categorie':
            afficheRessourcesCategories($_GET['id']);
            break;

        case 'ressource':
            listRessource();
            break;

        case 'insert':
            $lien = $_POST['lien'];
            $nom = $_POST['nom'];
            $descip = $_POST['description'];
            $cat = $_POST['Categorie'];
            Ajouter($lien, $nom, $descip, $cat);
            header('Location: /PHPMVC/ressource');
            exit();
            break;

        case 'login':
            $mdp = $_POST['mdp'];
            $nom = $_POST['nom'];
            Utiliteur($nom, $mdp);
            header('Location: /PHPMVC/ressource');
            break;

        case 'modifieVal':
            $lien = $_POST['lien'];
            $nom = $_POST['nom'];
            $descip = $_POST['description'];
            $id = $_POST['MonId'];
            faireModif($lien, $nom, $descip, $id);
            header('Location: /PHPMVC/ressource');
            break;
    }
}
